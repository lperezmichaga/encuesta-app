package bo.udi.encuestaapp.Modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

public class ClsRespuesta {
    @SerializedName("enuncuiado")
    @Expose
    private String idRespuesta;

    @SerializedName("enuncuiado")
    @Expose
    private String idPregunta;

    @SerializedName("enuncuiado")
    @Expose
    private int poscol;

    @SerializedName("enuncuiado")
    @Expose
    private int posfila;

    @SerializedName("enuncuiado")
    @Expose
    private String nombreRespuesta;

    private int IdTipoRespuesta;

    public  ClsRespuesta(){

    }

    public  ClsRespuesta (String id){
        idRespuesta = UUID.randomUUID().toString();
        idPregunta = id;
        poscol = 0;
        posfila = 0;
        nombreRespuesta = "Rellenar";
    }

    public int getIdTipoRespuesta() {
        return IdTipoRespuesta;
    }

    public void setIdTipoRespuesta(int idTipoRespuesta) {
        IdTipoRespuesta = idTipoRespuesta;
    }

    public String getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(String idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public int getPoscol() {
        return poscol;
    }

    public void setPoscol(int poscol) {
        this.poscol = poscol;
    }

    public int getPosfila() {
        return posfila;
    }

    public void setPosfila(int posfila) {
        this.posfila = posfila;
    }

    public String getNombreRespuesta() {
        return nombreRespuesta;
    }

    public void setNombreRespuesta(String nombreRespuesta) {
        this.nombreRespuesta = nombreRespuesta;
    }
}
