package bo.udi.encuestaapp.Modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClsEncuesta {
    @SerializedName("descripcion")
    @Expose
    private String idEncuesta;

    @SerializedName("descripcion")
    @Expose
    private String descripcion;

    @SerializedName("estado")
    @Expose
    private boolean estado;

    @SerializedName("enuncuiado")
    @Expose
    private String nombreEncuesta;

    @SerializedName("enuncuiado")
    @Expose
    private String idUsuarioCreador;

    private List<ClsPregunta> listaPregunta;

    public String getIdEncuesta() {
        return idEncuesta;
    }

    public void setIdEncuesta(String idEncuesta) {
        this.idEncuesta = idEncuesta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getNombreEncuesta() {
        return nombreEncuesta;
    }

    public void setNombreEncuesta(String nombreEncuesta) {
        this.nombreEncuesta = nombreEncuesta;
    }

    public String getIdUsuarioCreador() {
        return idUsuarioCreador;
    }

    public void setIdUsuarioCreador(String idUsuarioCreador) {
        this.idUsuarioCreador = idUsuarioCreador;
    }

    public List<ClsPregunta> getListaPregunta() {
        return listaPregunta;
    }

    public void setListaPregunta(List<ClsPregunta> listaPregunta) {
        this.listaPregunta = listaPregunta;
    }
}
