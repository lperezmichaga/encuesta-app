package bo.udi.encuestaapp.Modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClsUsuario {

    @SerializedName("Login")
    @Expose
    private String usuarioId;

    @SerializedName("Pass")
    @Expose
    private String password;

    @SerializedName("Codigo")
    @Expose
    private String cargo;

    @SerializedName("Apellido")
    @Expose
    public String apellido;
    @SerializedName("Nombre")
    @Expose
    private String nombre;
}
