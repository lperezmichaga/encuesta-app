package bo.udi.encuestaapp.Modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClsCrearEncuesta {
    @SerializedName("Login")
    @Expose
    private int encuestaId;
    @SerializedName("Login")
    @Expose
    private String tituloEncuest;
    @SerializedName("Login")
    @Expose
    private int cantitdadRespondidas;

    public ClsCrearEncuesta(int encuestaId, String tituloEncuest, int cantitdadRespondidas) {
        this.encuestaId = encuestaId;
        this.tituloEncuest = tituloEncuest;
        this.cantitdadRespondidas = cantitdadRespondidas;
    }

    public int getEncuestaId() {
        return encuestaId;
    }

    public void setEncuestaId(int encuestaId) {
        this.encuestaId = encuestaId;
    }

    public String getTituloEncuest() {
        return tituloEncuest;
    }

    public void setTituloEncuest(String tituloEncuest) {
        this.tituloEncuest = tituloEncuest;
    }

    public int getCantitdadRespondidas() {
        return cantitdadRespondidas;
    }

    public void setCantitdadRespondidas(int cantitdadRespondidas) {
        this.cantitdadRespondidas = cantitdadRespondidas;
    }
}
