package bo.udi.encuestaapp.Modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClsPregunta {
    @SerializedName("enunciado")
    @Expose
    private String nombrPregunta;

    @SerializedName("enuncuiado")
    @Expose
    private String fechaCreacion;

    @SerializedName("enuncuiado")
    @Expose
    private String idPregunta;

    @SerializedName("enuncuiado")
    @Expose
    private int idtipoPregunta;

    @SerializedName("enuncuiado")
    @Expose
    private boolean obligatorio;

    public ClsPregunta(){
        idtipoPregunta = 0;
    }

    private List<ClsRespuesta> listaRespuesta;

    public String getNombrPregunta() {
        return nombrPregunta;
    }

    public void setNombrPregunta(String nombrPregunta) {
        this.nombrPregunta = nombrPregunta;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public int getIdtipoPregunta() {
        return idtipoPregunta;
    }

    public void setIdtipoPregunta(int idtipoPregunta) {
        this.idtipoPregunta = idtipoPregunta;
    }

    public boolean isObligatorio() {
        return obligatorio;
    }

    public void setObligatorio(boolean obligatorio) {
        this.obligatorio = obligatorio;
    }

    public List<ClsRespuesta> getListaRespuesta() {
        return listaRespuesta;
    }

    public void setListaRespuesta(List<ClsRespuesta> listaRespuesta) {
        this.listaRespuesta = listaRespuesta;
    }
}
