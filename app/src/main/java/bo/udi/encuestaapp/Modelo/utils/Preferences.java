package bo.udi.encuestaapp.Modelo.utils;

import android.content.SharedPreferences;

/**
 * Created by Marcelo on 15/9/2017.
 */

public class Preferences {

    public static String getPassPreferences(SharedPreferences preferences) {
        return preferences.getString("password", "");
    }
    public static String getUserPreferences(SharedPreferences preferences) {
        return preferences.getString("nombreUsuario", "");
    }
    public static String getNombreUsuario(SharedPreferences preferences) {
        return preferences.getString("nombreUsuario", "");
    }

    public static String getApellidoUsuario(SharedPreferences preferences) {
        return preferences.getString("nombreUsuario", "");
    }
    public static String getIdUsuario(SharedPreferences preferences) {
        return preferences.getString("idUsuario", "");
    }
    public static String getTipoUsuario(SharedPreferences preferences) {
        return preferences.getString("tipoUsuario", "");
    }
    public static int getColor(SharedPreferences preferences,String SiglaMateria) {
        int prefers = 0;
        prefers = preferences.getInt(SiglaMateria, 0);

        return prefers;
    }

}
