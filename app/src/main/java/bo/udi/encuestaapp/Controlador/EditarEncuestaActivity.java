package bo.udi.encuestaapp.Controlador;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import bo.udi.encuestaapp.Controlador.Adapter.Adapter_RV_NavEditarEncuesta;
import bo.udi.encuestaapp.Controlador.Fragments.EditarEncuesta.MenuEditarEncuestaFragment;
import bo.udi.encuestaapp.Controlador.Fragments.Main.Adapter.Adapter_RV_CrearEncuestaFragment;
import bo.udi.encuestaapp.Controlador.interfaces.RecyclerView_ClickListener;
import bo.udi.encuestaapp.Modelo.ClsPregunta;
import bo.udi.encuestaapp.R;

public class EditarEncuestaActivity extends AppCompatActivity  implements RecyclerView_ClickListener {

    private RecyclerView recyclerViewn;
    private Adapter_RV_NavEditarEncuesta adaptern;
    private boolean CargoTodo;
    private List<ClsPregunta> listaPregunta;
    private int posSelected;
    private String IdEncuesta;
    private LinearLayout btnPRellenar,btnPMultiple,btnPDesplegable,vMenu;
    private FrameLayout vcontainer;
    private TextView txtEstadoNav;
    private ImageView  btne;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_encuesta);
        InitComponents();
        mostrarFragment();
    }
    public void InitComponents(){
        IdEncuesta = (getIntent() != null) ?  getIntent().getExtras().getString("IdEncuesta") : "" ;

        btnPRellenar = (LinearLayout)findViewById(R.id.btnAEEPRellenar);
        btnPDesplegable = (LinearLayout)findViewById(R.id.btnAEEPDesplegable);
        btnPMultiple = (LinearLayout)findViewById(R.id.btnAEEPMultiple);
        txtEstadoNav = (TextView)findViewById(R.id.AEEtxtMarcadorNavegacion);

        btnPRellenar.setOnClickListener(clickop);btnPMultiple.setOnClickListener(clickop);btnPDesplegable.setOnClickListener(clickop);

        vMenu = (LinearLayout)findViewById(R.id.AEEMenuPregunta) ;
        vcontainer = (FrameLayout)findViewById(R.id.AEEFragmentContaniner);
        btne = (ImageView)findViewById(R.id.btnAEEEliminar);
        btne.setOnClickListener(delete);

    }
    private View.OnClickListener delete = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(listaPregunta.size()>1) {
                RemoveAllFragments(getSupportFragmentManager());
                listaPregunta.remove(posSelected);
                posSelected = posSelected-1;
                adaptern.setSelectePosicion(posSelected);
                adaptern.notifyDataSetChanged();
                CargarVista();
            }
        }
    };
    private View.OnClickListener clickop = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view == btnPRellenar){
                listaPregunta.get(posSelected).setIdtipoPregunta(1);
            }
            else if(view == btnPDesplegable){
                listaPregunta.get(posSelected).setIdtipoPregunta(2);
            }
            else if(view == btnPMultiple){
                listaPregunta.get(posSelected).setIdtipoPregunta(3);
            }
            CargarVista();
        }
    };
    public void mostrarFragment(){
        listaPregunta = new ArrayList<>();
        listaPregunta.add(new ClsPregunta());

        recyclerViewn = (RecyclerView) findViewById(R.id.rv_caceditencuesta);
        final LinearLayoutManager layoutManagerI = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerViewn.setLayoutManager(layoutManagerI);
        adaptern = new Adapter_RV_NavEditarEncuesta(this, listaPregunta,0);
        recyclerViewn.setAdapter(adaptern);
        adaptern.setClickListener(this);
    }

    @Override
    public void onClick(View view, int position) {
        if(position == listaPregunta.size()){
            AddNewFragment();
        }
        adaptern.setSelectePosicion(position);
        int p= position+1;
        txtEstadoNav.setText("Pregunta "+p+" de "+listaPregunta.size());
        posSelected = position;
        CargarVista();

    }
    public void CargarVista(){
        if(listaPregunta.get(posSelected).getIdtipoPregunta() <= 0){
            vcontainer.setVisibility(View.GONE);
            vMenu.setVisibility(View.VISIBLE);

        }
        else {
            vMenu.setVisibility(View.GONE);
            vcontainer.setVisibility(View.VISIBLE);
            MenuEditarEncuestaFragment menuf =new MenuEditarEncuestaFragment();
            menuf.setPregunta(listaPregunta.get(posSelected));
            addFragment(menuf,"asda");
        }
    }
    public void CommitChanges(ClsPregunta preg){
        RemoveAllFragments(getSupportFragmentManager());
        listaPregunta.set(posSelected,preg);
        CargarVista();
    }
    public void RemoveFragment(){
        RemoveAllFragments(getSupportFragmentManager());
        listaPregunta.get(posSelected).setIdtipoPregunta(0);
        CargarVista();
    }


    public void addFragment(Fragment currentFragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.AEEFragmentContaniner,currentFragment,tag)
                .addToBackStack(tag)
                .commit();
    }
    public void addOnFragment(Fragment currentFragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.AEEFragmentContaniner,currentFragment,tag)
                .commit();

    }
    public void AddNewFragment(){
        listaPregunta.add(new ClsPregunta());
        adaptern.notifyDataSetChanged();
    }
    public  boolean recursivePopBackStack(FragmentManager fragmentManager) {
        if (fragmentManager.getFragments() != null) {
            for (Fragment fragment : fragmentManager.getFragments()) {
                if (fragment != null && fragment.isVisible()) {
                    boolean popped = recursivePopBackStack(fragment.getChildFragmentManager());
                    if (popped) {
                        return true;
                    }
                }
            }
        }
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();

            return true;
        }

        return false;
    }

    public void RemoveAllFragments(FragmentManager fragmentManager){
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
            if (fragmentManager.getBackStackEntryCount() > 0) {
                fragmentManager.popBackStack();
            }
        }
    }
    public void ShowAlertdialog(String Titulo,String detalle){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setTitle(Titulo);

        dialog.setMessage(detalle);
        dialog.show();

    }
    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()>0){
            RemoveFragment();
        }
        else {
            ShowAlertdialog("Alerta","Desea Salir sin Guardar Cambio");
        }
    }

    public void btnCancelarPregunta(View view) {
        ShowAlertdialog("Alerta","Desea Salir sin Guardar Cambio");
    }

    public void btnAceptarPregunta(View view) {
        GuardarDatos();
    }
    public void GuardarDatos(){

    }
}
