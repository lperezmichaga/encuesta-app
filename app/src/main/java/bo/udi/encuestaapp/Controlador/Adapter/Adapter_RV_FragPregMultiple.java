package bo.udi.encuestaapp.Controlador.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import bo.udi.encuestaapp.Controlador.interfaces.RecyclerView_ClickListener;
import bo.udi.encuestaapp.Modelo.ClsPregunta;
import bo.udi.encuestaapp.Modelo.ClsRespuesta;
import bo.udi.encuestaapp.R;

/**
 * Created by luisp on 28/5/2018.
 */

public class Adapter_RV_FragPregMultiple extends RecyclerView.Adapter<Adapter_RV_FragPregMultiple.ViewHolder> {
    public Context context;
    public List<ClsRespuesta> cateList;
    public List<Boolean> lestado;
    public int resmultiple;
    private RecyclerView_ClickListener clickListener;

    public Adapter_RV_FragPregMultiple(Context context, List<ClsRespuesta> catList,List<Boolean> checked) {
        this.context = context;
        this.cateList = catList;
        this.lestado = checked;

    }

    @Override
    public Adapter_RV_FragPregMultiple.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_resmultiple, parent, false);
        if(cateList.get(0).getIdTipoRespuesta()==3){
            resmultiple = 3;
        }else
        if(cateList.get(0).getIdTipoRespuesta()==4) {
            resmultiple =4; }
        return new Adapter_RV_FragPregMultiple.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Adapter_RV_FragPregMultiple.ViewHolder holder, int position) {

        if(resmultiple == 3){
            holder.contenedoradd.setVisibility(View.GONE);
            holder.contenedor.setVisibility(View.VISIBLE);
            holder.txtRespuestaA.setText(cateList.get(position).getNombreRespuesta());
            holder.txtRespuestaA.setChecked(lestado.get(position));
        }
        else if(resmultiple == 4){
            holder.contenedoradd.setVisibility(View.VISIBLE);
            holder.contenedor.setVisibility(View.GONE);
            holder.txtRespuestaB.setText(cateList.get(position).getNombreRespuesta());
            holder.txtRespuestaB.setChecked(lestado.get(position));
        }
    }
    @Override
    public int getItemCount() {
        return cateList.size();
    }


    public void setClickListener(RecyclerView_ClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public RadioButton txtRespuestaA;
        public CheckBox txtRespuestaB;
        public LinearLayout contenedor,contenedoradd;

        public ViewHolder(View itemView) {
            super(itemView);
            txtRespuestaA = (RadioButton) itemView.findViewById(R.id.cardRM_radiobutton);
            txtRespuestaB = (CheckBox) itemView.findViewById(R.id.cardRM_check);
            contenedor = (LinearLayout) itemView.findViewById(R.id.carRM_container);
            contenedoradd = (LinearLayout) itemView.findViewById(R.id.carRM_containeradd);

            contenedoradd.setTag(contenedor);
            contenedoradd.setOnClickListener(this);
            contenedor.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

}