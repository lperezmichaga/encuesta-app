package bo.udi.encuestaapp.Controlador.Fragments.Main.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import bo.udi.encuestaapp.Controlador.interfaces.RecyclerView_ClickListener;
import bo.udi.encuestaapp.Modelo.ClsCrearEncuesta;
import bo.udi.encuestaapp.R;

/**
 * Created by luisp on 28/5/2018.
 */

public class Adapter_RV_CrearEncuestaFragment extends RecyclerView.Adapter<Adapter_RV_CrearEncuestaFragment.ViewHolder> {
    public Context context;
    public List<ClsCrearEncuesta> cateList;
    private RecyclerView_ClickListener clickListener;

    public Adapter_RV_CrearEncuestaFragment(Context context, List<ClsCrearEncuesta> catList) {
        this.context = context;
        this.cateList = catList;
    }

    @Override
    public Adapter_RV_CrearEncuestaFragment.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_crearencuestas, parent, false);
        return new Adapter_RV_CrearEncuestaFragment.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Adapter_RV_CrearEncuestaFragment.ViewHolder holder, int position) {
        //holder.NombreCarrera.setText(cateList.get(position).getNombreCarrera());
        holder.Titulo.setText(cateList.get(position).getTituloEncuest());
        holder.Respondida.setText(cateList.get(position).getCantitdadRespondidas()+" Respondidas");


    }
    @Override
    public int getItemCount() {
        return cateList.size();
    }

    public void setcambioEstado(List<ClsCrearEncuesta> cateListNew){
        cateList = cateListNew;
        this.notifyDataSetChanged();
    }
    public void setClickListener(RecyclerView_ClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView Titulo,Respondida;
        public LinearLayout contenedor;

        public ViewHolder(View itemView) {
            super(itemView);
            Titulo = (TextView) itemView.findViewById(R.id.cardce_txtTitulo);
            Respondida = (TextView) itemView.findViewById(R.id.carde_txtRespondida);
            contenedor = (LinearLayout) itemView.findViewById(R.id.cardce_container);
            contenedor.setTag(contenedor);
            contenedor.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}