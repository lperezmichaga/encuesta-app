package bo.udi.encuestaapp.Controlador.Fragments.EditarEncuesta;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import bo.udi.encuestaapp.Controlador.Adapter.Adapter_RV_FragPregMultiple;
import bo.udi.encuestaapp.Controlador.Adapter.Adapter_RV_MostrarFragPregMultiple;
import bo.udi.encuestaapp.Controlador.Adapter.Adapter_RV_NavEditarEncuesta;
import bo.udi.encuestaapp.Controlador.EditarEncuestaActivity;
import bo.udi.encuestaapp.Controlador.interfaces.RecyclerView_ClickListener;
import bo.udi.encuestaapp.Modelo.ClsPregunta;
import bo.udi.encuestaapp.Modelo.ClsRespuesta;
import bo.udi.encuestaapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreguntaMultipleFragment extends Fragment implements RecyclerView_ClickListener {

    private ClsPregunta pregunta;
    private  View mview;
    private RecyclerView recyclerView;
    private Adapter_RV_MostrarFragPregMultiple adapter;
    private List<ClsRespuesta> listarespuesta;
    private EditText txtPregunta;
    private RadioGroup radioGroup;
    private Switch estado;
    private Button btnAceptar,btnCancelar;

    public PreguntaMultipleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_preguntamultiple, container, false);
        InitComponents();
        mostrarFragment();
        return  mview;
    }
    private void InitComponents(){
        radioGroup = (RadioGroup)mview.findViewById(R.id.rbgFPM);
        estado = mview.findViewById(R.id.swFPRestado);
        txtPregunta = mview.findViewById(R.id.txtFPMtitulo);
        btnAceptar = mview.findViewById(R.id.btnFPMAceptar);
        btnCancelar = mview.findViewById(R.id.btnFPMCancelar);
        btnAceptar.setOnClickListener(clickAceptar);
        btnCancelar.setOnClickListener(clickCancelar);

        if(pregunta.getListaRespuesta()==null){
            listarespuesta = new ArrayList<>();
        }
        else{
            listarespuesta = pregunta.getListaRespuesta();
        }
    }
    public void mostrarFragment(){
        listarespuesta = new ArrayList<>();

        recyclerView = (RecyclerView) mview.findViewById(R.id.rv_fpreguntamultiple);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new Adapter_RV_MostrarFragPregMultiple(getContext(), listarespuesta);
        recyclerView.setAdapter(adapter);
        adapter.setClickListener(this);
    }
    public void setPregunta(ClsPregunta pregunt) {
        this.pregunta = pregunt;
    }
    private View.OnClickListener clickAceptar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Aceptar();
        }
    };
    public void Aceptar(){
        if(!txtPregunta.getText().toString().trim().isEmpty()){
            pregunta.setIdPregunta(UUID.randomUUID().toString());
            pregunta.setNombrPregunta(txtPregunta.getText().toString());
            pregunta.setObligatorio(estado.isChecked());
            anadirTipoRespuesta();
            pregunta.setListaRespuesta(listarespuesta);
            ((EditarEncuestaActivity)getActivity()).CommitChanges(pregunta);
        }
    }
    public void anadirTipoRespuesta(){
        int tres = 0;
        switch (radioGroup.getCheckedRadioButtonId()){
            case R.id.rbFPMsunica:
                tres = 3;
                break;
            case R.id.rbFPMsmultiple:
                tres = 4;
                break;
        }
        for (int i = 0; i < listarespuesta.size(); i++) {
            listarespuesta.get(i).setIdTipoRespuesta(tres);
        }

    }


    private View.OnClickListener clickCancelar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //add opcion cambiar idtipo
            ((EditarEncuestaActivity)getActivity()).RemoveFragment();
        }
    };


    @Override
    public void onClick(View view, int position) {
        if(position == listarespuesta.size()) {
            showChangeLangDialog(position, "Añadir", "Escriba la Respuesta que desea añadir");
        }else{
            listarespuesta.remove(position);
            adapter.notifyDataSetChanged();
        }
        ///AQUI ME QUEDE
    }

    public void showChangeLangDialog(final int pos,String Titulo,String Mensja) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.card_alertdialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        dialogBuilder.setTitle(Titulo);
        dialogBuilder.setMessage(Mensja);
        dialogBuilder.setNeutralButton("AÑADIR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
               if( validarRespuesta(edt.getText().toString().trim(),pos)){
                   ClsRespuesta respuesta= new ClsRespuesta();
                   respuesta.setNombreRespuesta(edt.getText().toString().trim());
                   respuesta.setIdPregunta(pregunta.getIdPregunta());
                   respuesta.setIdRespuesta(UUID.randomUUID().toString());
                   respuesta.setPoscol(0);
                   respuesta.setPosfila(pos);
                   listarespuesta.add(respuesta);
                   adapter.notifyDataSetChanged();
                   dialog.dismiss();
               }
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.setCancelable(false);
        b.setCanceledOnTouchOutside(false);
        b.show();
    }
    public boolean validarRespuesta(String res,int pos){
        if(res.trim().isEmpty()){
            return false;
        }else{
            return  true;
        }
    }
}
