package bo.udi.encuestaapp.Controlador.interfaces;

import android.view.View;

public interface RecyclerView_ClickListener {

    void onClick(View view, int position);

}