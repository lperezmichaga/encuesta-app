package bo.udi.encuestaapp.Controlador.Fragments.EditarEncuesta;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import bo.udi.encuestaapp.Controlador.EditarEncuestaActivity;
import bo.udi.encuestaapp.Modelo.ClsPregunta;
import bo.udi.encuestaapp.Modelo.ClsRespuesta;
import bo.udi.encuestaapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreguntaRellenarFragment extends Fragment {

    public  ClsPregunta pregunta;
    private  View mview;
    private Switch estado;
    private Button btnAceptar,btnCancelar;
    private List<ClsRespuesta> listaRespuesta;
    private EditText txtPregunta;

    public PreguntaRellenarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_pregunta_rellenar, container, false);
        InitComponent();
        return  mview;
    }
    public void InitComponent(){
        estado = mview.findViewById(R.id.swFPRestado);
        txtPregunta = mview.findViewById(R.id.txtFPRtitulo);
        btnAceptar = mview.findViewById(R.id.btnAFRAceptar);
        btnCancelar = mview.findViewById(R.id.btnAFRCancelar);
        btnAceptar.setOnClickListener(clickAceptar);
        btnCancelar.setOnClickListener(clickCancelar);

        if(pregunta.getListaRespuesta()==null){
            listaRespuesta = new ArrayList<>();
        }
        else{
            listaRespuesta = pregunta.getListaRespuesta();
        }
    }
    public void InitData(){

    }
    private View.OnClickListener clickAceptar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Aceptar();
        }
    };
    public void Aceptar(){
        if(!txtPregunta.getText().toString().trim().isEmpty()){
            pregunta.setIdPregunta(UUID.randomUUID().toString());
            pregunta.setNombrPregunta(txtPregunta.getText().toString());
            pregunta.setObligatorio(estado.isChecked());
            listaRespuesta.add(new ClsRespuesta(pregunta.getIdPregunta()));
            pregunta.setListaRespuesta(listaRespuesta);
            ((EditarEncuestaActivity)getActivity()).CommitChanges(pregunta);
        }
    }


    private View.OnClickListener clickCancelar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //add opcion cambiar idtipo
            ((EditarEncuestaActivity)getActivity()).RemoveFragment();
        }
    };

    public void setPregunta(ClsPregunta pregunt) {
        this.pregunta = pregunt;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

}
