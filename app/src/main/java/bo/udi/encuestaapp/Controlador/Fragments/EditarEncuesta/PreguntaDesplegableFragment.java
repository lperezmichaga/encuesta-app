package bo.udi.encuestaapp.Controlador.Fragments.EditarEncuesta;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bo.udi.encuestaapp.Modelo.ClsPregunta;
import bo.udi.encuestaapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreguntaDesplegableFragment extends Fragment {

    private  ClsPregunta pregunta;
    private  View mview;
    public PreguntaDesplegableFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_preguntadesplegable, container, false);
        return  mview;
    }
    public void setPregunta(ClsPregunta pregunt) {
        this.pregunta = pregunt;
    }


}
