package bo.udi.encuestaapp.Controlador.RETROFIT;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Marcelo on 14/9/2017.
 */


public interface UdiService {

    ///////////LOGIN /////////
    @GET("Login")
    Call<Respuesta<String >> login(@Query("key1") String usr,@Query("key2") String pass,@Query("key3") String key);

}
