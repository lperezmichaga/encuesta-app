package bo.udi.encuestaapp.Controlador;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import bo.udi.encuestaapp.Controlador.Fragments.Main.AcercaDeFragment;
import bo.udi.encuestaapp.Controlador.Fragments.Main.ConfiguracionFragment;
import bo.udi.encuestaapp.Controlador.Fragments.Main.CrearEncuestasFragment;
import bo.udi.encuestaapp.Controlador.Fragments.Main.EcuestaFragment;
import bo.udi.encuestaapp.R;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new EcuestaFragment();
                    break;
                case R.id.navigation_encuesta:
                    fragment = new CrearEncuestasFragment();
                    break;
                case R.id.navigation_configuracion:
                    fragment = new ConfiguracionFragment();
                    break;
               /* case R.id.navigation_acercade:
                    fragment = new AcercaDeFragment();
                    break;*/

            }
            return loadFragment(fragment);
        }
    };
    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mainfragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new EcuestaFragment());

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        //navigation.getMenu().removeItem(R.id.navigation_encuesta);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
