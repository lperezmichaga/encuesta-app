package bo.udi.encuestaapp.Controlador.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import bo.udi.encuestaapp.Controlador.interfaces.RecyclerView_ClickListener;
import bo.udi.encuestaapp.Modelo.ClsRespuesta;
import bo.udi.encuestaapp.R;

/**
 * Created by luisp on 28/5/2018.
 */

public class Adapter_RV_MostrarFragPregMultiple extends RecyclerView.Adapter<Adapter_RV_MostrarFragPregMultiple.ViewHolder> {
    public Context context;
    public List<ClsRespuesta> cateList;
    private RecyclerView_ClickListener clickListener;

    public Adapter_RV_MostrarFragPregMultiple(Context context, List<ClsRespuesta> catList) {
        this.context = context;
        this.cateList = catList;

    }

    @Override
    public Adapter_RV_MostrarFragPregMultiple.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_fragpreguntamultiple, parent, false);
        return new Adapter_RV_MostrarFragPregMultiple.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Adapter_RV_MostrarFragPregMultiple.ViewHolder holder, int position) {
        //holder.NombreCarrera.setText(cateList.get(position).getNombreCarrera());
        int pos = position+1;
        if(position == cateList.size()){
            holder.contenedoradd.setVisibility(View.VISIBLE);
            holder.contenedor.setVisibility(View.GONE);
        }
        else {
            holder.contenedoradd.setVisibility(View.GONE);
            holder.contenedor.setVisibility(View.VISIBLE);
            holder.txtRespuesta.setText(cateList.get(position).getNombreRespuesta());
            holder.txtRespuesta.setSelected(true);
        }

    }
    @Override
    public int getItemCount() {
        return cateList.size()+1;
    }

    public void setcambioEstado(List<ClsRespuesta> cateListNew){
        cateList = cateListNew;
        this.notifyDataSetChanged();
    }
    public void setClickListener(RecyclerView_ClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtRespuesta;
        public LinearLayout contenedor,contenedoradd,delete;

        public ViewHolder(View itemView) {
            super(itemView);
            txtRespuesta = (TextView) itemView.findViewById(R.id.cardfpm_txt);
            contenedor = (LinearLayout) itemView.findViewById(R.id.cardfmp_container);
            contenedoradd = (LinearLayout) itemView.findViewById(R.id.cardfmp_containeradd);
            delete = (LinearLayout) itemView.findViewById(R.id.cardfpm_click);
            contenedoradd.setTag(contenedor);
            contenedoradd.setOnClickListener(this);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

}