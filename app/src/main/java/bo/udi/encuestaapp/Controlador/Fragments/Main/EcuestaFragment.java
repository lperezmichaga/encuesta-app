package bo.udi.encuestaapp.Controlador.Fragments.Main;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bo.udi.encuestaapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EcuestaFragment extends Fragment {


    public EcuestaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ecuesta, container, false);
    }

}
