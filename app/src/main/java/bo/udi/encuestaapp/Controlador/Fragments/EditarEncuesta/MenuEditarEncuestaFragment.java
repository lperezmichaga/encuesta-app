package bo.udi.encuestaapp.Controlador.Fragments.EditarEncuesta;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bo.udi.encuestaapp.Controlador.Adapter.Adapter_RV_FragPregMultiple;
import bo.udi.encuestaapp.Controlador.Adapter.Adapter_RV_MostrarFragPregMultiple;
import bo.udi.encuestaapp.Controlador.Adapter.Adapter_RV_NavEditarEncuesta;
import bo.udi.encuestaapp.Controlador.EditarEncuestaActivity;
import bo.udi.encuestaapp.Controlador.interfaces.RecyclerView_ClickListener;
import bo.udi.encuestaapp.Modelo.ClsPregunta;
import bo.udi.encuestaapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuEditarEncuestaFragment extends Fragment implements RecyclerView_ClickListener {

    private ClsPregunta pregunta;
    private View mview;
    private FrameLayout container;
    private Fragment fragmentElegido;
    private List<Boolean> lestadoMultiple;
    private RecyclerView recyclerView;
    private Adapter_RV_FragPregMultiple adapter;
    private NestedScrollView vPreguntaRelleno,vPreguntaMultiple,vPreguntaDesplegable;

    private TextView vPRtxttitulo,vPMtxttitulo;
    public MenuEditarEncuestaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(mview == null) {
            mview = inflater.inflate(R.layout.fragment_menu_editarencuesta, container, false);
            initComponent();

            ComprobarPregunta();
        }
        else{

        }
        return  mview;
    }
    public void initComponent(){
        container = (FrameLayout)mview.findViewById(R.id.FMEE_fragmentcontainer);
        vPreguntaRelleno = (NestedScrollView)mview.findViewById(R.id.FMEEPreguntaRellenar);
        vPreguntaMultiple = (NestedScrollView)mview.findViewById(R.id.FMEEPreguntaMultiple);
        vPRtxttitulo = (TextView)mview.findViewById(R.id.FMMEEPRtitulo);
        vPMtxttitulo = (TextView)mview.findViewById(R.id.FMMEEPMtitulo);
    }

    public void ComprobarPregunta(){
        if(pregunta.getNombrPregunta()==null){
            GoneAllview();
            container.setVisibility(View.VISIBLE);
            int idTipoPreg = pregunta.getIdtipoPregunta();
            switch (idTipoPreg){
                case 1:
                    PreguntaRellenarFragment preg1 = new PreguntaRellenarFragment();
                    preg1.setPregunta(pregunta);
                    fragmentElegido = preg1;
                    break;
                case 2:
                    PreguntaDesplegableFragment preg2 = new PreguntaDesplegableFragment();
                    preg2.setPregunta(pregunta);
                    fragmentElegido = preg2;
                    break;
                case 3:
                    PreguntaMultipleFragment preg3 = new PreguntaMultipleFragment();
                    preg3.setPregunta(pregunta);
                    fragmentElegido = preg3;
                    break;
            }
            ((EditarEncuestaActivity)getActivity()).addFragment(fragmentElegido,"PreguntaF");

        }
        else{
            int idTipoPreg = pregunta.getIdtipoPregunta();
            switch (idTipoPreg){
                case 1:
                    LoadPReguntaRellenar();
                    break;
                case 2:

                    break;
                case 3:
                    LoadPReguntaMultiple();
                    break;
            }
        }
    }
    public void LoadPReguntaRellenar(){
        GoneAllview();
        vPreguntaRelleno.setVisibility(View.VISIBLE);
        vPreguntaRelleno.setEnabled(false);
        vPRtxttitulo.setText(pregunta.getNombrPregunta());
    }
    public void LoadPReguntaMultiple(){
        GoneAllview();
        vPreguntaMultiple.setVisibility(View.VISIBLE);
        vPreguntaMultiple.setEnabled(false);
        vPMtxttitulo.setText(pregunta.getNombrPregunta());
        lestadoMultiple = new ArrayList<>();
        initListBoolena();
        mostrarFragmentMultiple();
    }

    public void mostrarFragmentMultiple(){

        recyclerView = (RecyclerView) mview.findViewById(R.id.rv_FMEEEmultiple);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new Adapter_RV_FragPregMultiple(getContext(),pregunta.getListaRespuesta(),lestadoMultiple);
        recyclerView.setAdapter(adapter);
        adapter.setClickListener(this);
    }
    public void GoneAllview(){
        container.setVisibility(View.GONE);
        vPreguntaRelleno.setVisibility(View.GONE);
        //vPreguntaDesplegable.setVisibility(View.GONE);
        vPreguntaMultiple.setVisibility(View.GONE);
    }
    public void initListBoolena(){
        for (int i = 0; i < pregunta.getListaRespuesta().size(); i++) {
            lestadoMultiple.add(false);
        }
    }
    public void replaceBoolena(){
        for (int i = 0; i < pregunta.getListaRespuesta().size(); i++) {
            lestadoMultiple.set(i,false);
        }
    }

    public void setPregunta(ClsPregunta pregunt) {
        this.pregunta = pregunt;
    }

    @Override
    public void onClick(View view, int position) {
        if(pregunta.getListaRespuesta().get(position).getIdTipoRespuesta()==3){
            replaceBoolena();
            lestadoMultiple.set(position,true);
            adapter.notifyDataSetChanged();
        }
        else if (pregunta.getListaRespuesta().get(position).getIdTipoRespuesta()==4){
            lestadoMultiple.set(position,!lestadoMultiple.get(position));
            adapter.notifyDataSetChanged();
        }
    }
}
