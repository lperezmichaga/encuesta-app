package bo.udi.encuestaapp.Controlador;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.UUID;

import bo.udi.encuestaapp.Modelo.ClsEncuesta;
import bo.udi.encuestaapp.Modelo.utils.Preferences;
import bo.udi.encuestaapp.R;

public class CrearEncuestaActivity extends AppCompatActivity {

    private ClsEncuesta encuesta;
    private EditText txtnombre;
    private LinearLayout btnEncuesta,btnResultado,btnDelete;
    private SharedPreferences prefsLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crearencuesta);
        initComponets();
        cargarArgumentsandPrefer();
        encuesta = new ClsEncuesta();
    }

    public void initComponets(){
        txtnombre = (EditText)findViewById(R.id.txtACEnombre);
        btnEncuesta = (LinearLayout)findViewById(R.id.btnACEEditarEnc);
        btnResultado = (LinearLayout)findViewById(R.id.btnACEResultados);
        btnEncuesta.setOnClickListener(onclick);
        btnResultado.setOnClickListener(onclick);
    }
    public void cargarArgumentsandPrefer(){
        prefsLogin = this.getSharedPreferences("PrefsLogin", Context.MODE_PRIVATE);
    }
    private View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view == btnEncuesta){
                Intent in  = new Intent(CrearEncuestaActivity.this ,EditarEncuestaActivity.class);
                in.putExtra("Id","");
                startActivity(in);
            }
            else if (view == btnResultado){
                Intent in  = new Intent(CrearEncuestaActivity.this,ResultadoEncuestaActivity.class);
                in.putExtra("Id","");
                startActivity(in);
            }
            else if (view == btnResultado){
                //Implementar para borrar
            }
        }
    };

    public void btnAceptarNombre(View view) {
        if(txtnombre.length() == 0 || txtnombre.getText().toString().trim().isEmpty()){
            ShowAlertDialogOK("Alerta","El campo no puede quedar vacio",false);
        }
        else{
            RegistrarEncuesta();
            view.setVisibility(View.GONE);
        }
    }
    public void RegistrarEncuesta(){
        txtnombre.setFocusable(false);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtnombre.getWindowToken(), 0);
        encuesta.setNombreEncuesta(txtnombre.getText().toString());
        encuesta.setIdEncuesta(UUID.randomUUID().toString());
        encuesta.setIdUsuarioCreador(Preferences.getIdUsuario(prefsLogin));
        encuesta.setEstado(false);
        //AQUI EL SERVICIO PARAREGISTRAR LA ENCUESTA
    }

    public void ShowAlertDialogOK(String Titulo, String Detalle, final Boolean closeActivity){
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                this).create();

        alertDialog.setTitle(Titulo);

        alertDialog.setMessage(Detalle);

        alertDialog.setButton(Dialog.BUTTON_NEUTRAL,"OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if(closeActivity) {
                    finish();
                }
            }
        });
        alertDialog.show();
    }

}
