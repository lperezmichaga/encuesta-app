package bo.udi.encuestaapp.Controlador.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import bo.udi.encuestaapp.Controlador.interfaces.RecyclerView_ClickListener;
import bo.udi.encuestaapp.Modelo.ClsCrearEncuesta;
import bo.udi.encuestaapp.Modelo.ClsPregunta;
import bo.udi.encuestaapp.R;

/**
 * Created by luisp on 28/5/2018.
 */

public class Adapter_RV_NavEditarEncuesta extends RecyclerView.Adapter<Adapter_RV_NavEditarEncuesta.ViewHolder> {
    public Context context;
    public List<ClsPregunta> cateList;
    public  int posSelect;
    private RecyclerView_ClickListener clickListener;

    public Adapter_RV_NavEditarEncuesta(Context context, List<ClsPregunta> catList,int pos) {
        this.context = context;
        this.cateList = catList;
        this.posSelect = pos;
    }

    @Override
    public Adapter_RV_NavEditarEncuesta.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_naveditarencuesta, parent, false);
        return new Adapter_RV_NavEditarEncuesta.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Adapter_RV_NavEditarEncuesta.ViewHolder holder, int position) {
        //holder.NombreCarrera.setText(cateList.get(position).getNombreCarrera());
        int pos = position+1;
        if(position == cateList.size()){
            holder.Nro.setText("+");
        }
        else {
            holder.Nro.setText(pos+"");
        }
        if(posSelect==position){
            holder.Nro.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
            holder.Nro.setTextColor(context.getResources().getColor(R.color.blanco));
        }
        else{
            holder.Nro.setTextColor(context.getResources().getColor(R.color.negro));
            holder.Nro.setBackgroundColor(context.getResources().getColor(R.color.blanco));
        }


    }
    @Override
    public int getItemCount() {
        return cateList.size()+1;
    }

    public void setcambioEstado(List<ClsPregunta> cateListNew){
        cateList = cateListNew;
        this.notifyDataSetChanged();
    }
    public void setClickListener(RecyclerView_ClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView Nro;
        public LinearLayout contenedor;

        public ViewHolder(View itemView) {
            super(itemView);
            Nro = (TextView) itemView.findViewById(R.id.cardnee_txtTitulo);
            Nro.setBackgroundColor(context.getResources().getColor(R.color.blanco));
            contenedor = (LinearLayout) itemView.findViewById(R.id.cardnee_container);
            contenedor.setTag(contenedor);
            contenedor.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
    public void setSelectePosicion(int pos){
        this.posSelect = pos;
        notifyDataSetChanged();
    }
}