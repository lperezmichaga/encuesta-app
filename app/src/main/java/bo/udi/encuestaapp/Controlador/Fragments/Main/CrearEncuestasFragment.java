package bo.udi.encuestaapp.Controlador.Fragments.Main;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import bo.udi.encuestaapp.Controlador.CrearEncuestaActivity;
import bo.udi.encuestaapp.Controlador.Fragments.Main.Adapter.Adapter_RV_CrearEncuestaFragment;
import bo.udi.encuestaapp.Controlador.interfaces.RecyclerView_ClickListener;
import bo.udi.encuestaapp.Controlador.utils.Alert_Dialog_P;
import bo.udi.encuestaapp.Modelo.ClsCrearEncuesta;
import bo.udi.encuestaapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrearEncuestasFragment extends Fragment implements RecyclerView_ClickListener {
    List<ClsCrearEncuesta> listaencuestac;
    Boolean CargoTodo;
    private RecyclerView recyclerView;
    private Adapter_RV_CrearEncuestaFragment adapter;
    Alert_Dialog_P pdialog;
    View mview;
    public CrearEncuestasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(mview==null) {
            listaencuestac = new ArrayList<>();
            mview = inflater.inflate(R.layout.fragment_crear_encuestas, container, false);
            FloatingActionButton fab = (FloatingActionButton) mview.findViewById(R.id.fabaddPreguntas);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goCreateEncuesta();
                }
            });

            loadData();
        }
        return mview;
    }
    public void goCreateEncuesta(){
        startActivity(new Intent(getActivity(),CrearEncuestaActivity.class));
    }

    public void loadData(){
        if(listaencuestac==null){
            listaencuestac = new ArrayList<>();
        }
        listaencuestac.add(new ClsCrearEncuesta(1,"Encuesta 1",2));
        listaencuestac.add(new ClsCrearEncuesta(2,"Encuesta 2",0));
        listaencuestac.add(new ClsCrearEncuesta(3,"Encuesta 3",1));
        listaencuestac.add(new ClsCrearEncuesta(4,"Encuesta 4",3));
        listaencuestac.add(new ClsCrearEncuesta(5,"Encuesta 5",1));
        listaencuestac.add(new ClsCrearEncuesta(6,"Encuesta 6",0));
        CargoTodo = true;
        mostrarFragment();
    }
    public void mostrarFragment(){
        try {
            if (CargoTodo) {
                recyclerView = (RecyclerView) mview.findViewById(R.id.rv_crearencuesta);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter = new Adapter_RV_CrearEncuestaFragment(getActivity(), listaencuestac);
                recyclerView.setAdapter(adapter);
                adapter.setClickListener(this);
            } else {

            }
        }catch (Exception e){

        }
    }
    public void ShowAlert(String mensaje) {
        Alert_Dialog_P customDialog = new Alert_Dialog_P(getActivity());
        customDialog.setTitulo("Alerta");
        customDialog.setContenido(mensaje);
        customDialog.enableNeutralButton(true);
        customDialog.setTextNeutralButton("Aceptar");
        customDialog.show();
    }

    @Override
    public void onClick(View view, int position) {

    }
}
